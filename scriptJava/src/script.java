import java.io.File;
import java.io.FileReader;
import java.io.IOException; 
import java.io.*;


public class script{
    
    public static String readFile(String filename)
    {
        String content = null;
        File file = new File(filename); // For example, foo.txt
        FileReader reader = null;
        try {
            reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return content;
    }

    public static void genererStaff(String[] args) throws Exception{
        //création du fichier html
        File f = new File("staff.html");
        //récupération du fichier txt
        String fichierTxt = readFile("fichier-txt/staff.txt");
        //Mettre le fichier dans un tableau
        String[] lines = fichierTxt.split("\r\n|\r|\n");
        //écriture du html
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        bw.write("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
        bw.write("<link rel='icon' type='image/png' href='assets/logo-transparent.png' />");
        bw.write("<link rel='stylesheet' href='css/bootstrap/bootstrap.css'>");
        bw.write("<link rel='stylesheet' href='css/style.css'>");
        bw.write("<ul class='horizontal'>");
        bw.write("<li><a href='staff.html'>Agents</a></li>");
        bw.write("<li><a href='liste.html'>&Eacutequipements</a></li>");
        bw.write("<img id='logo' name='logo' src='assets/logo.png' />");
        bw.write("</ul>");
        bw.write("</head><body>");
        bw.write("<h1>Liste du staff</h1>");
        //insertion du fichier txt
        bw.write("<div id='listeStaff'>");
        for (int ii=0; ii< lines.length; ii++) {
            bw.write("<p><a href='"+lines[ii]+".html'>"+ lines[ii] +"</a></p>");
        }
        bw.write("</div>");
        bw.write("</body></html>");
        bw.close();
    }
    
    public static void genererListe(String[] args) throws Exception{
        File f = new File("liste.html");
        String fichierTxt = readFile("fichier-txt/liste.txt");
        String[] lines = fichierTxt.split("\r\n|\r|\n");
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        bw.write("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
        bw.write("<link rel='icon' type='image/png' href='assets/logo-transparent.png' />");
        bw.write("<link rel='stylesheet' href='css/bootstrap/bootstrap.css'>");
        bw.write("<link rel='stylesheet' href='css/style.css'>");
        bw.write("<ul class='horizontal'>");
        bw.write("<li><a href='staff.html'>Agents</a></li>");
        bw.write("<li><a href='liste.html'>&Eacutequipements</a></li>");
        bw.write("<img id='logo' name='logo' src='assets/logo.png' />");
        bw.write("</ul>");
        bw.write("</head><body>");
        bw.write("<h1>Liste des &eacutequipements</h1>");
        bw.write("<div id='listeEquipements'>");
        for (int ii=0; ii< lines.length; ii++) {
            lines[ii] = lines[ii].replaceFirst(" ", ": ");
            bw.write("<p>"+ lines[ii] +"</p>");
        }
        bw.write("</div>");
        bw.write("</body></html>");
        bw.close();
    }

    public static void genererCarteAgent(String[] args, String fichier, String nomAgent) throws Exception{
        File f = new File(nomAgent + ".html");
        String fichierTxt = readFile(fichier);
        String[] lines = fichierTxt.split("\r\n|\r|\n");
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        bw.write("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
        bw.write("<link rel='icon' type='image/png' href='assets/logo-transparent.png' />");
        bw.write("<link rel='stylesheet' href='css/bootstrap/bootstrap.css'>");
        bw.write("<link rel='stylesheet' href='css/style.css'>");
        bw.write("<ul class='horizontal'>");
        bw.write("<li><a href='staff.html'>Agents</a></li>");
        bw.write("<li><a href='liste.html'>&Eacutequipements</a></li>");
        bw.write("<img id='logo' name='logo' src='assets/logo.png' />");
        bw.write("</ul>");
        bw.write("</head><body>");
        bw.write("<div id='identity' name='identity'>");
        String idCard = lines[1].toLowerCase().charAt(0) + lines[0].toLowerCase();
        bw.write("<img id='idCard' name='idCard' src='idCards/"+ idCard +".png' />");
        bw.write("</div>");
        bw.write("<div id='information' name='information'>");
        bw.write("<h1>Votre fiche</h1>");
        bw.write("<p>Nom : "+ lines[0] +"</p>");
        bw.write("<p>Pr&eacutenom : "+ lines[1] +"</p>");
        bw.write("<p>Affectation : "+ lines[2] +"</p>");
        //bw.write("<p>Mot de passe : "+ lines[3] +"</p>");
        bw.write("</div>");
        bw.write("<div id='equipements' name='equipements'>");
        bw.write("<h2>Vos &eacutequipements :</h2>");
        for (int ii=4; ii< lines.length; ii++) {
            bw.write("<p>"+ lines[ii] +"</p>");
        }
        bw.write("</div>");
        bw.write("</body></html>");
        bw.close();
    }

    public static void main(String[] args) throws Exception {
        genererStaff(args);
        genererListe(args);

        //récupération du fichier txt
        String fichierTxt = readFile("fichier-txt/staff.txt");
        //Mettre le fichier dans un tableau
        String[] lines = fichierTxt.split("\r\n|\r|\n");
        for (String line : lines) {
            genererCarteAgent(args, "fichier-txt/"+ line +".txt", line);
        }
    }

    

}